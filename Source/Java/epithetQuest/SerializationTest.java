/* 
 * Copyright © 2015 Nathan Johnson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.io.*;

/** Currently only a serialization test. */
public class SerializationTest {

    /** @param arguments Unused. */
    public static void main(String[] arguments) throws Exception {
	File store = new File("store");
	OutputStream os = new FileOutputStream(store);
	ObjectOutput oOut = new ObjectOutputStream(os);
	Dictionary lIn, lOut;
	lOut = new Dictionary();
	oOut.writeObject(lOut);
	InputStream is = new FileInputStream(store);
	ObjectInputStream ois = new ObjectInputStream(is);
	lIn = (Dictionary) ois.readObject();
	System.out.println(lIn);
    }

}
