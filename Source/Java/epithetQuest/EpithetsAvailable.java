/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import static epithetQuest.Dictionary.dictionary;
import java.util.TreeSet;

/**
 Domain of Epithets available. Arranged in a tree structure. It constructs and
 provides a global data structure.

 @author Nathan Johnson
 */
public class EpithetsAvailable extends TreeSet<Epithet> {

    /** Global tree of epithets. */
    public static EpithetsAvailable epithetsAvailable = new EpithetsAvailable();

    /** Hard coded test case structure. */
    EpithetsAvailable() {
	Epithet previous, next;
	previous = new Epithet();
	previous.set(dictionary.find(Language.ENG, "DATA DEVICE"));
	add(previous);
	next = new Epithet();
	next.set(dictionary.find(Language.ENG, "INPUT"));
	Epithet mouse = new Epithet();
	mouse.set(dictionary.find(Language.ENG, "MOUSE"));
	next.leafward.add(mouse);
	previous.leafward.add(next);
	next = new Epithet();
	next.set(dictionary.find(Language.ENG, "OUTPUT"));
	previous.leafward.add(next);
	Epithet headphones = new Epithet();
	headphones.set(dictionary.find(Language.ENG, "HEADPHONES"));
	next.leafward.add(headphones);
    }
}
