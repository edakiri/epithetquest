/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

/**
 A table of all Prose in all Languages. Another implementation is
 {@link DictionaryPanel}. Uses {@link JTable}

 @see javax.swing.JTable
 @author Nathan Johnson
 *//* Note: a JScrollPane is needed to get table column headings. */

public class DictionaryTableModel extends AbstractTableModel implements
    TableModelListener,
    ListSelectionListener {

    protected Prose.StringBox[][] tableDisplayText;
    protected View view;

    public static void createGUI() {
	JFrame frame = SwingUtility.basicFrame("Dictionary Table Test");
	JPanel panel = (JPanel) frame.getContentPane();
	JComponent component = new DictionaryTableView();
//	component = new JScrollPane(component);//To get Scroll Bar
	panel.add(component);
	frame.pack();
    }

    /** Entry point to demonstrate this component only.

     @param nothing Unused.
     @throws java.lang.Exception Anything throws. */
    public static void main(String nothing[]) throws Exception {
	assert nothing.length == 0;
	javax.swing.SwingUtilities.
	    invokeAndWait(DictionaryTableModel::createGUI);
    }

    public DictionaryTableModel() {
	addTableModelListener(this);
    }

    @Override
    public int getColumnCount() {
	return Language.values().length;
    }

    @Override
    public String getColumnName(int columnIndex) {
	return Language.values()[columnIndex].toString();
    }

    @Override
    public int getRowCount() {
	return Dictionary.dictionary.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
	Prose prose = (Prose) Dictionary.dictionary.toArray()[rowIndex];
	Language language = Language.values()[columnIndex];
	Prose.StringBox proseString = prose.get(language);
	return proseString;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
	return true;
    }

    @Override
    public void setValueAt(Object theValue, int rowIndex, int columnIndex) {
	Prose.StringBox proseString = (Prose.StringBox) getValueAt(rowIndex,
	    columnIndex);
	proseString.set((String) theValue);
    }

    @Override
    public void tableChanged(TableModelEvent event) //No events received.
    {
	System.err.println("tableChanged");
	assert event.getSource() == this;
	int typeOfEvent = event.getType();
	if (typeOfEvent != TableModelEvent.UPDATE) //work update of single cell
	{
	    return;
	}
	int rowIndex = event.getFirstRow();
	assert rowIndex == event.getLastRow();//expect only 1 row updated
	int columnIndex = event.getColumn();
	String columnName = getColumnName(columnIndex);
	System.out.println(getValueAt(rowIndex,
	    columnIndex).getClass());
	System.out.println(getValueAt(rowIndex, columnIndex));
	Prose.StringBox proseString = (Prose.StringBox) getValueAt(rowIndex,
	    columnIndex);

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
	System.err.println("valueChanged");
    }

}
