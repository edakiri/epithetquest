/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.util.*;

/* ToDo: change from Runnable so that we get the caller. */
public class ProseView implements Runnable, Comparable<ProseView>,
    CallerAnonymous {

    final protected Set<Runnable> listeners = new LinkedHashSet<>(
	Language.values().length);
    final Prose prose;//contained so that it can be listened to.
    final View view;

    public ProseView(Prose prose, View view) {
	this.prose = prose;
	prose.listeners().add(this);
	this.view = view;
	view.listeners().add(this);
    }

    @Override
    public int compareTo(ProseView o) {
	int comparison = view.language.compareTo(o.view.language);
	if (comparison == 0) {
	    comparison = prose.compareTo(o.prose);
	}
	return comparison;
    }

    @Override
    public Set<Runnable> listeners() {
	return listeners;
    }

    /** Called to notify of change. Notifies listeners in turn. If we identify
     the caller, we can reduce notifications.
     */
    @Override
    public void run() {
	for (Runnable listener : listeners) {
	    listener.run();
	}
    }

    @Override
    public String toString() {
	return prose.get(view).toString();
    }
}
