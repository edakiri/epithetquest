/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.logging.*;

/** All {@link Prose}. Entries are unique {@link Prose}. Loads from and saves to
 a file, Dictionary.txt There is only 1 dictionary at this time, though that
 could change since they can be persisted.

 @see Prose
 @author Nathan Johnson
 */
public class Dictionary extends TreeSet<Prose> implements java.io.Serializable {

    /** The global dictionary. */
    public static Dictionary dictionary = new Dictionary();

    public Dictionary() {
	load();
    }

    /** Add Prose to the dictionary.

     @param p Prose to add to the dictionary.
     @return true if an only if element is added. */
    @Override
    public boolean add(Prose p) {
	if (p.isEmpty()) {
	    return false;
	}
	@SuppressWarnings("UnusedAssignment")
	boolean result = false;
	try {
	    result = super.add(p);
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
	return result;
    }

    /** Find an entry of Prose in the Dictionary if you know its language and
     text.

     @param l Language of the search phrase or text.
     @param speech The phrase or a text representation which is being sought.
     @return The found prose or null if it is missing.
     */
    public Prose find(Language l, String speech) {
	for (Prose p : this) {
	    if (p.get(l).compareTo(speech) == 0) {
		return p;
	    }
	}
	return null;
    }

    /** Load the dictionary from the file Dictionary.txt . Prose of each
     language is separated from others by a tabulator character. Entries are in
     the order of the enum {@link Language}. */
    public void load() {
	Path path = Paths.get("Dictionary.txt");
	try {
	    List<String> lines = Files.readAllLines(path);
	    for (String line : lines) {
		Prose prose = new Prose();
		String[] prosePerLanguage = line.split("\t");
		for (Language language : Language.values()) {
		    prose.put(language, prosePerLanguage[Language.integer(
			language)]);
		}
		add(prose);
	    }
	} catch (java.nio.file.NoSuchFileException exception) {
	    System.err.println("The following file is missing.\n" + path
		+ "\nIt must be in the current directory to run this program.");
	    System.exit(1);

	} catch (IOException ex) {
	    Logger.getLogger(Dictionary.class.getName()).log(
		Level.SEVERE, null,
		ex);
	    System.exit(1);
	}
    }
}
