/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JTextField;

/** Modifiable text field, associated with a {@link View}, {@link Language}, and
 {@link Prose}.

 @author Nathan Johnson
 */
public class TextField extends JTextField implements ActionListener, Runnable {

    protected final Prose.StringBox proseString;

    //ToDo: Traverse up container heirarchy to get frame.
    private static void createGUI() {
	JFrame frame = SwingUtility.basicFrame("Text Field Test");
	Container contentPane = frame.getContentPane();

	View view = new View();
	Prose prose = Dictionary.dictionary.find(Language.ENG, "INPUT");
	Prose.StringBox string = prose.get(Language.ENG);
	JTextField textField = new TextField(string);
	contentPane.add(textField);
	contentPane.add(new LLabel(new ProseView(prose, view)));
	frame.pack();
    }

    /** Entry point to demonstrate this component only.

     @param nothing No parameters processed.
     @throws java.lang.Exception Any error ends. */
    public static void main(String nothing[]) throws Exception {
	assert nothing.length == 0;
	javax.swing.SwingUtilities.invokeAndWait(TextField::createGUI);
    }

    public TextField(Prose prose, Language language) {
	proseString = prose.get(language);
	super.setText(proseString.toString());
	addActionListener(this);
    }

    public TextField(Prose.StringBox prose) {
	proseString = prose;
	super.setText(proseString.toString());
	prose.listeners.add(this);//ToDo: use this.
	addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	proseString.set(this.getText());
	System.out.println(this.getText());
	SwingUtility.pack(this);
    }

    @Override
    public void run() {
	super.setText(proseString.toString());
	SwingUtility.pack(this);
    }

}
