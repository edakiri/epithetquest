/*
 * Copyright © 2015 Nathan Johnson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import static epithetQuest.Dictionary.dictionary;
import java.awt.GridLayout;
import javax.swing.JPanel;

/** A table of all Prose in all Languages. Another implementation is
 {@link DictionaryTableView}.

 @author Nathan Johnson
 */
public class DictionaryPanel extends JPanel {

    DictionaryPanel(View view) {
	setLayout(new GridLayout(dictionary.size(),
	    Language.values().length));
	for (Prose p : dictionary) {
	    for (Language l : Language.values()) {
		Prose.StringBox stringBox = p.get(l);
		TextField textField = new TextField(p, l);
		add(textField);
	    }
	}
    }

}
