/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import javax.swing.tree.DefaultMutableTreeNode;

/** This is to be used to provide updates to the GUI when the Prose or
 View.language changes.

 @author Nathan Johnson
 */
public class EpithetTreeNode extends DefaultMutableTreeNode implements Runnable {

    protected EpithetDisplaySet model;

    public EpithetTreeNode(EpithetView epithetView, EpithetDisplaySet model) {
	this.model = model;
	epithetView.get().listeners.add(this);
	super.setUserObject(epithetView);
    }

    @Override
    public void run() {
	model.accept(this);
    }

}
