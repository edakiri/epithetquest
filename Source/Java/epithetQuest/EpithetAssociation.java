/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

/** Associates an epithet an object. In this case, the object is currently an
 image. Since notifying others of change to this association is not supported,
 changing this association is also not supported. */
public class EpithetAssociation implements Comparable<EpithetAssociation> {

    /** An Image associated with the Epithet. */
    public final SwungImage labelled;
    /** An Epithet associated with the Image. */
    public final Epithet tag;

    public EpithetAssociation(Epithet tag, SwungImage image) {
	this.tag = tag;
	this.labelled = image;
    }

    /** ToDo: implement tree comparison for Epithet. Otherwise, only the single
     epithet is compared and not the path from root. That is incorrect.

     @param oc Other tree.
     @return Ordering. */
    @Override
    public int compareTo(EpithetAssociation oc) {
	int tagCompare = tag.compareTo(oc.tag);
	if (tagCompare != 0) {
	    return tagCompare;
	}
	int imageCompare = labelled.fileName.compareTo(oc.labelled.fileName);
	return imageCompare;
    }
}
