/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.util.TreeMap;

/** Makes 2 SortedMaps Comparable. Is unused.

 @see Compare
 @author Nathan Johnson
 */
public class ComparableMap<K, V extends Comparable<?>> extends TreeMap<K, V>
    implements Comparable<ComparableMap<K, V>> {

    /** ToDo: Move this into a general comparable enum map? */
    @Override
    public int compareTo(ComparableMap<K, V> co) {
	for (K k : this.keySet()) {
	    int comparison = ((Comparable) this.get(k)).compareTo(co.get(k));
	    if (comparison != 0) {
		return comparison;
	    }
	}
	return 0;
    }
}
