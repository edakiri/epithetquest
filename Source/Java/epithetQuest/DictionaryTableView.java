/*
 * Copyright © 2015 Nathan Johnson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.Scrollable;

/** Needed to set the preferred size for JScrollPane. Otherwise, it sets the
 vertical size wrong.

 @author Nathan Johnson
 */
public class DictionaryTableView extends JTable implements Scrollable {

    public DictionaryTableView() {
	super(new DictionaryTableModel());
    }

    @Override //of Scrollable
    public Dimension getPreferredScrollableViewportSize() {
	Dimension preferredSize = super.getPreferredSize();
	preferredSize.width += preferredSize.width >> 1;//Fix for broken Swing
	return preferredSize;
    }

    /*
     @Override //of Scrollable\\ public int getScrollableUnitIncrement(Rectangle
     visibleRect, int orientation, int direction) { throw new
     UnsupportedOperationException("Not supported yet."); }

     @Override //of Scrollable\\ public int
     getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int
     direction) { throw new UnsupportedOperationException("Not supported yet.");
     }

     @Override //of Scrollable\\ public boolean
     getScrollableTracksViewportWidth() { throw new
     UnsupportedOperationException("Not supported yet."); }

     @Override //of Scrollable\\ public boolean
     getScrollableTracksViewportHeight() { throw new
     UnsupportedOperationException("Not supported yet."); }
     */
}
