/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

/**
 Simple identification of a language. Enum is the ISO 639-3 representation.

 - [ISO639-3](http://www-01.sil.org/iso639-3/codes.asp?order=lang_type&letter=c)
 - [IETF](http://tools.ietf.org/rfc/bcp/bcp47.txt)

 @author Nathan Johnson
 */
public enum Language implements java.io.Serializable {

    /** German */
    DEU,
    /** English en-GB-oed */
    ENG;

    /** Japanese */
//    JPN,
    /** Lojban - [Language codes](http://www.lojban.org/tiki/Language+Code) */
    //   JBO
    /** Find the index of the specified language.

     @param language language to be converted into an associated integer value.
     @return Value representing the index of the Language in the enumeration. */
    static public int integer(Language language) {
	return java.util.Arrays.binarySearch(Language.values(), language);
    }
}
