/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.util.function.Function;

/** Has the use of a tag or label in a heirarchy. */
public class Epithet extends TreeJoint<Prose> {

    public EpithetView constructTree(EpithetView leftTrunkward,
	Function<Prose, ProseView> function) {

	Prose rightBurden = get();
	EpithetView leftTreeJoint = new EpithetView();
	leftTreeJoint.trunkward = leftTrunkward;
	leftTreeJoint.set(function.apply(rightBurden));
	for (TreeJoint<Prose> leafwardJointProse : leafward) {
	    Epithet leafwardJoint = (Epithet) leafwardJointProse;
	    EpithetView treeJointC = leafwardJoint.constructTree(leftTreeJoint,
		function);
	    leftTreeJoint.leafward.add(treeJointC);
	}
	return leftTreeJoint;
    }

    @Override
    public void run() {
    }

}
