/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import javax.swing.JLabel;

/** A text label which reflects Prose according to a
 {@link View} {@link Language}. It listens to messages about changes and then
 changes the GUI accordingly. Language Label.

 @author Nathan Johnson
 */
public class LLabel extends JLabel implements Comparable<Object>, Runnable {

    protected Language language;//Current language. Gets updated according to changes in View.
    protected final ProseView proseView;

    public LLabel(ProseView proseView) {
	super(proseView.toString());
	this.proseView = proseView;
	language = proseView.view.language;
	proseView.listeners().add(this);
    }

    /** Compare as String.

     @param o Object to be compared to.
     @return Whether before, same, or after according to sort. */
    @Override
    public int compareTo(Object o) {
	return toString().compareTo(o.toString());
    }

    /** Event is either View changing language or change to prose. */
    @Override
    public void run() {
	this.setText(proseView.toString());
	SwingUtility.pack(this);
    }

}
