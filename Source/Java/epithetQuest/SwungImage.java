/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/** An image, which is loaded from a file. Provides an image in file as an AWT
 GUI element. */
public class SwungImage extends java.awt.Component implements
    Comparable<SwungImage> {

    protected BufferedImage img;
    public final String fileName;
    public int linkCount;

    /** @param fileName Name of the file from which we shall load this image. */
    public SwungImage(String fileName) {
	this.fileName = fileName;
	try {
	    System.out.println("Creating image " + fileName);
	    File file = new File(fileName);
	    img = ImageIO.read(file);
	    System.out.println("done");
	} catch (IOException e) {
	    e.printStackTrace();
	    System.exit(1);
//ToDo: Better error handling.
	}

    }

    @Override
    public void paint(Graphics g) {
	g.drawImage(img, 0, 0, null);
    }

    @Override
    public Dimension getPreferredSize() {
	return new Dimension(img.getWidth(null), img.getHeight(null));
    }

    @Override
    /** Compare by file name. */
    public int compareTo(SwungImage oi) {
	return fileName.compareTo(oi.fileName);
    }
}
