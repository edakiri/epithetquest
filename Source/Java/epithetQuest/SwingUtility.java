/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/** Things used in common to help with Swing.

 @author Nathan Johnson
 */
public abstract class SwingUtility {

    public static void pack(java.awt.Container container) {
	java.awt.Container previous;
	//Looks like this is same as getWindowAncestor
/*
	 do { previous = container; container = container.getParent(); } while
	 (container != null); */

	previous = javax.swing.SwingUtilities.getWindowAncestor(container);
	((java.awt.Window) previous).pack();
//boolean assignableFrom=container.getClass().isAssignableFrom(java.awt.Window.class);
    }

    public static JFrame basicFrame(String title) {
	JFrame frame = new JFrame(title);
	frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	frame.setVisible(true);
	Container contentPane = frame.getContentPane();
	BoxLayout layout = new BoxLayout(contentPane, BoxLayout.PAGE_AXIS);
	contentPane.setLayout(layout);
	return frame;
    }

    private SwingUtility() {
    }
}
