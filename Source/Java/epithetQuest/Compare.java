/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedMap;

/** Makes a collection comparable with others.

 ToDo: Remove redundant implementations of container comparison methods among
 classes.

 @param c Contained collection to which is delegeted.
 @see ComparableMap
 @see ArrayCompare
 @author Nathan Johnson
 */
public class Compare<T> implements Collection<T>, Comparable<Collection<T>> {

    Collection<T> collection;

    public Compare(Collection<T> c) {
	this.collection = c;
    }

    static <K, V> int compare(SortedMap<K, V> a, SortedMap<K, V> b) {

	int comparison;
	comparison = compare(a.size(), b.size());
	if (comparison != 0) {
	    return comparison;
	}

	comparison = compare(a.keySet(), b.keySet());
	if (comparison != 0) {
	    return comparison;
	}

	comparison = compare(a.values(), b.values());
	return comparison;
    }

    static int compare(int a, int b) {
	return a - b;
    }

    static <C> int compare(Collection<C> a, Collection<C> b) {
	int comparison;//reused for comparisons
	comparison = compare(a.size(), b.size());
	if (comparison != 0) {
	    return comparison;
	}

	Iterator<C> ai = a.iterator();
	Iterator<C> bi = b.iterator();

	while (ai.hasNext()) {
	    comparison = ((Comparable) ai.next()).compareTo(bi.next());
	    if (comparison != 0) {
		return comparison;
	    }
	}
	return 0;
    }

    @Override
    public int size() {
	return collection.size();
    }

    @Override
    public boolean isEmpty() {
	return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
	return collection.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
	return collection.iterator();
    }

    @Override
    public Object[] toArray() {
	return collection.toArray();
    }

    @Override
    public Object[] toArray(Object[] a) {
	return collection.toArray(a);
    }

    @Override
    public boolean add(T e) {
	return collection.add(e);
    }

    @Override
    public boolean remove(Object o) {
	return collection.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
	return collection.containsAll(c);
    }

    @Override
    public boolean addAll(Collection c) {
	return collection.addAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
	return collection.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection c) {
	return collection.retainAll(c);
    }

    @Override
    public void clear() {
	collection.clear();
    }

    @Override
    public int compareTo(Collection<T> o) {
	return compare(this, (Collection) o);
    }

}
