/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 Nexus in a tree structure. The metaphor is that the aggregate resembles the
 part of atree above the ground. At each joint, connected branches can either
 lead towards the trunk or leaf ends. The ends away from the trunk are leaves.

 @author Nathan Johnson
 @param <R> Contained thing which is {@link java.util.Comparable} to others of
 its type.
 */
//ToDo: maybe would rather make this extend SortedSet.
public class TreeJoint<R extends Comparable<R> & CallerAnonymous> implements
    Comparable<TreeJoint<R>>, Runnable {

    /** Content which is used to order the joints. */
    private R burden;

    public SortedSet<TreeJoint<R>> leafward = new TreeSet<>();

    protected final Set<Runnable> listeners = new LinkedHashSet<>(4);
    /** Branch in the direction of the root of the tree. */
    public TreeJoint<R> trunkward;

    /** Compares burdens. ToDo: implement tree path comparison rather than only
     burden.

     @param o Other which sent the message and shall be compared to this.
     @return comparison of burdens.
     */
    @Override
    public int compareTo(TreeJoint<R> o) {
	return burden.compareTo(o.burden);
    }

    R get() {
	return burden;
    }

    public void set(R burden) {
	this.burden = burden;
	burden.listeners().add(this);
    }

    @Override
    public void run() {
	throw new UnsupportedOperationException("Not supported yet.");
    }

}
