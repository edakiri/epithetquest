package epithetQuest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.testng.annotations.*;

/**
 @author Nathan Johnson
 */
public class CompareTest {

    public CompareTest() {
    }

    @Test
    public void different() {
	Integer[] a = {1, 2};
	Integer[] b = {1, 3};
	List<Integer> al = Arrays.asList(a);
	List<Integer> bl = Arrays.asList(b);
	Compare<Integer> ac = new Compare<>(al);
	Compare<Integer> bc = new Compare<>(bl);
	assert (Compare.compare(ac, bc) < 0);
	assert (Compare.compare(bc, ac) > 0);
    }

    @Test
    public void empty() {
	ArrayList<Object> a = new ArrayList<>(0);
	ArrayList<Object> b = new ArrayList<>(0);
	assert (Compare.compare(a, b) == 0);
    }

    @Test
    public void longer() {
	ArrayList<Integer> a = new ArrayList<>(1);
	a.add(3);
	ArrayList<Integer> b = new ArrayList<>(0);
	assert (Compare.compare(a, b) > 0);
	assert (Compare.compare(b, a) < 0);
    }

}
