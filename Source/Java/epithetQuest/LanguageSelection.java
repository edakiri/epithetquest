/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/** A graphical element to select a language. ToDo: alter to select a list of
 languages.

 @author Nathan Johnson
 */
public class LanguageSelection extends JPanel implements ActionListener {

    protected View view;

    /** Currently selects language. Make generic later. */
    LanguageSelection(View view) {
	this.view = view;
	Language[] buttons = Language.values();
	ButtonGroup group = new ButtonGroup();
	this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	for (Language item : buttons) {
	    JRadioButton radioButton = new JRadioButton(item.toString());
	    group.add(radioButton);
	    this.add(radioButton);
	    if (item == view.language) {
		radioButton.setSelected(true);
	    }
	    radioButton.addActionListener(this);
	}
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	Language l = Language.valueOf(e.getActionCommand());
	view.language(l);
    }
}
