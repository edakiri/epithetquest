/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import static epithetQuest.Dictionary.dictionary;
import static epithetQuest.Language.ENG;
import java.awt.Container;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.SwingUtilities.invokeAndWait;
import javax.swing.WindowConstants;

/** Set of graphical elements with shaled context. Must only be read by others.
 Multiple Views can operate on the same model. Contains the context for a View
 of Model, View, Controler, Model. GUI elements can ether get their language
 from the View or be bound to a specific Prose and Language combination. Prose
 and a Specific language combination is not registered here.

 View is not going to be an Abstract Factory unless some other services are
 added. If it were an Abstract Factory, it would need to be modified for any new
 listening element. If it is a Abstract Factory, it can directly keep pointers
 to all of its components to notify them. Currently, components only belong to 1
 View. View would be come an Abstract Factory if Serialization is implemented.

 @author Nathan Johnson
 */
public class View extends TreeSet<Runnable> implements Runnable,
    Function<Prose, ProseView>, Comparable<View>, CallerAnonymous {

    protected Container contentPane;
    protected JFrame frame;

    /** Reflects current language chosen for environment.

     ToDo: make languge preference a list. */
    public Language language = ENG;

    /** Things listening to changes to this view. */
    private final Set<Runnable> listeners = new LinkedHashSet<>(8);

    /**
     @param args Arguments are ignored.
     */
    public static void main(String[] args) throws Exception {
	View view = new View(Language.ENG);
	invokeAndWait(view::initialize);
    }

    protected View() {
    }

    protected View(Language l) {
	language = l;
    }

    @Override
    public ProseView apply(Prose prose) {
	ProseView proseView = new ProseView(prose, this);
	return proseView;
    }

    //This is not a complete sort, but is enough for ProseView.
    @Override
    public int compareTo(View o) {
	return language.compareTo(o.language);
    }

    public void initialize() {

	//Initialize GUI
	frame = new JFrame("Epithet Quest");
	frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	frame.setVisible(true);

	contentPane = frame.getContentPane();
	BoxLayout layout = new BoxLayout(contentPane, BoxLayout.PAGE_AXIS);
	contentPane.setLayout(layout);

	contentPane.add(new EpithetDisplaySet(this));

	contentPane.
	    add(new LLabel(new ProseView(dictionary.find(Language.ENG, "INPUT"),
		this)));
	contentPane.add(
	    new LLabel(new ProseView(dictionary.find(Language.ENG, "OUTPUT"),
		this)));
	contentPane.add(new LanguageSelection(this));
	JTable tableView = new DictionaryTableView();
	tableView.setFillsViewportHeight(true);
	JScrollPane scrollPane = new JScrollPane(tableView);//need for headings
	contentPane.add(scrollPane);//or DictionaryPanel
//	contentPane.add(new DictionaryPanel(this));//or DictionaryTableModel
	frame.pack();
    }

    void language(Language l) {
	language = l;
	for (Runnable r : listeners) {
	    r.run();
	}
	frame.pack();
    }

    @Override
    public Set<Runnable> listeners() {
	return listeners;
    }

    @Override
    public void run() {
	frame.pack();
	System.out.println("Pack");
    }

    @Override
    public String toString() {
	return "View with language " + language;
    }

}
