/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.awt.Container;
import java.util.function.Consumer;
import javax.swing.*;
import javax.swing.tree.*;

/** Tree structure representing a set of Epithets.

 @author Nathan Johnson
 */
public class EpithetDisplaySet extends JPanel implements
    Consumer<EpithetTreeNode> {

    private final DefaultTreeModel swingTree;

    /** Entry point to demonstrate this component only.

     @param nothing Not used. */
    public static void main(String nothing[]) {
	assert nothing.length == 0;
	JFrame frame = new JFrame("Epithet Display");
	frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	frame.setVisible(true);
	Container contentPane = frame.getContentPane();
	BoxLayout layout = new BoxLayout(contentPane, BoxLayout.PAGE_AXIS);
	contentPane.setLayout(layout);
	View view = new View(Language.ENG);
	JPanel epithetDisplaySet = new EpithetDisplaySet(view);
	contentPane.add(epithetDisplaySet);
	frame.pack();
    }

    public EpithetDisplaySet(View view) {
	Epithet trunkEpithet = EpithetsAvailable.epithetsAvailable.first();

	EpithetView epithetView
	    = trunkEpithet.constructTree(null, view);

	DefaultMutableTreeNode javaTreeRoot = constructTree(
	    epithetView);
	swingTree = new DefaultTreeModel(javaTreeRoot);
	JTree jTree = new JTree(swingTree);
	add(jTree);
    }

    @Override
    public void accept(EpithetTreeNode node) {
	System.out.println("Node changed: " + node);
	swingTree.nodeChanged(node);
    }

    /** Return a TreeNode for a JTree corresponding to the Epithet subtree.
     Constructs the visual tree recursively to mirror the Epithet tree.

     @param trunkward Node most towards or at the root or trunk of the tree.
     @return Subtree or if trunkward is null, trunk. */
    protected DefaultMutableTreeNode constructTree(EpithetView trunkward) {
	DefaultMutableTreeNode treeNode = new EpithetTreeNode(trunkward, this);
	for (TreeJoint<ProseView> epithetJoint : trunkward.leafward) {
	    EpithetView epithet = (EpithetView) epithetJoint;
	    treeNode.add(constructTree(epithet));
	}
	return treeNode;
    }
}
