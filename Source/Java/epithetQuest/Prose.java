/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.Set;

/** Uniquely represents a <em>meaning</em> or <em>deep structure</em> in sundry
 languages.

 @author Nathan Johnson
 */
public class Prose extends EnumMap<Language, Prose.StringBox>
    implements Comparable<Prose>, Serializable, CallerAnonymous, Runnable {

    final protected Set<Runnable> listeners = new LinkedHashSet<>(
	Language.values().length);

    Prose() {
	super(Language.class);
    }

    Prose(Prose other) {
	super(Language.class);
	putAll(other);
    }

    @Override
    public int compareTo(Prose co) {
	for (Language k : keySet()) {
	    String thisString = get(k).toString();
	    String thatString = co.get(k).toString();
	    int comparison = thisString.compareTo(thatString);
	    if (comparison != 0) {
		return comparison;
	    }
	}
	return 0;
    }

    /** Find which language corresponds to the StringBox. There can only be 1.
     This is a search of all elements until a match is found.

     @param sought Element to be found.
     @return Language matching the parameter.
     @exception java.util.NoSuchElementException That which was sought was not
     found.
     */
    public Language find(StringBox sought) {
	for (Language language : Language.values()) {
	    if (this.get(language) == sought) {
		return language;
	    }
	}
	throw new java.util.NoSuchElementException(
	    "StringBox does not exist:" + sought.string);
    }

    /** Performs a get of prose for the language using the language of the view.

     @param view View to use as context for the language.
     @return Match of language preference in View. */
    public StringBox get(View view) {
	return get(view.language);
    }

    @Override
    public Set<Runnable> listeners() {
	return listeners;
    }

    /** Used when building the dictionary from Strings. Otherwise, do not use.

     @param language Language associated with String for this Prose.
     @param string String associated with Language for this Prose.
     @return Association object between Language and String for a Prose.
     */
    public Prose.StringBox put(Language language, String string) {
	StringBox stringBox = get(language);
	if (stringBox == null) //If we have no text for this prose and language so far.
	{
	    stringBox = new StringBox(string);
	} else {
	    if (stringBox.string == string) //If same value and Object. Do nothing.
	    {
		return stringBox;
	    }
	    if (stringBox.toString().equals(string))//Perhaps a problem.
	    {
		throw new RuntimeException(
		    "A StringBox was set to the same value with another Object. Language="
		    + language + ". Value=" + string);

	    }
	}
	put(language, stringBox);
	return stringBox;
    }

    @Override
    public Prose.StringBox put(Language language, Prose.StringBox string) {
	super.put(language, string);
	run();
	return string;
    }

    @Override
    public void putAll(
	java.util.Map<? extends Language, ? extends Prose.StringBox> prose) {
	for (Entry<? extends Language, ? extends StringBox> entry : prose.
	    entrySet()) {
	    this.put(entry.getKey(), entry.getValue());
	}
    }

    /** Notify listeners of changes. */
    @Override
    public void run() {
	for (Runnable listener : listeners) {
	    listener.run();
	}
    }

    /** Binds a Language choice tegether with Prose. Provides notification when
     Prose changes. It is thus more specific than Prose.

     Provides a String and notification handle. The String is for GUI elements
     using String and setting the String results in notification according to
     the {@link Prose} and {@link Language}. This service could have been
     provided as a subclass of String or StringBuffer if they were not
     {@code final}. */
    public class StringBox implements Comparable<Object> {

	/** Most Listeners should be specific to Prose and not a language
	 because user language selection is going to be a list of preferred
	 languages.
	 */
	final public Set<Runnable> listeners = new LinkedHashSet<>(
	    Language.values().length);
	/** Only set through Prose. */
	private String string;

	private StringBox(String string) {
	    set(string);
	}

	/** Notify listeners of changes. */
	protected void call() {
	    for (Runnable listener : listeners) {
		listener.run();
	    }
	    Prose.this.run();
	}

	/** A comparison of the text value of the string. The Prose is not
	 considered.

	 @param other String to be compared by value.
	 @return String sort. */
	@Override
	public int compareTo(Object other) {
	    if (this == other || this.string == other) {
		return 0;
	    }
	    return string.compareTo(other.toString());
	}

	public void set(String string) {
	    assert string != null;
	    this.string = string;

	    call();
	}

	@Override
	public String toString() {
	    return string;
	}

    }
}
