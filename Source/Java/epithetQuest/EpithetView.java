/*
 Copyright © 2015 Nathan Johnson

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

/**
 Associates a View with Epithet. Output text according to combination.
 */
public class EpithetView extends TreeJoint<ProseView> implements Runnable {

    @Override
    public void run() {
	System.out.println("EpithetView changed " + toString());
	for (Runnable listener : listeners) {
	    listener.run();
	}
    }

    @Override
    public String toString() {
	return get().toString();
    }

}
