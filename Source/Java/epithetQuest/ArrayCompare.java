/* 
 * Copyright © 2015 Nathan Johnson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package epithetQuest;

/**
 Comparator function for arrays. It can order arrays so that one is greater that
 the other. Elements must be comparable. Since need disappeared, it handles only
 a static type for now, but can easily be extended. Can be used to implement
 {@link java.lang.Comparable}. If beginning array elements are the same, the
 longer array is greater.

 @see java.lang.Comparator
 @author Nathan Johnson
 */
public abstract class ArrayCompare {

    /** Does the comaprison as a Comparable does.

     @param thisString If it is greater, 1 is returned.
     @param thatString If it is greater, -1 is returned.
     */
    public static int compare(char[] thisString, char[] thatString) {

	int minimumLength = thisString.length > thatString.length
	    ? thatString.length : thisString.length;
	for (int i = 0; i < minimumLength; i++) {
	    if (thisString[i] < thatString[i]) {
		return -1;
	    } else if (thisString[i] > thatString[i]) {
		return 1;
	    }
	}
	if (thisString.length < thatString.length) {
	    return -1;
	}
	if (thisString.length > thatString.length) {
	    return 1;
	}
	return 0;
    }

}
